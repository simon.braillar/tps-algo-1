package s09;

import java.util.Arrays;

public class Ex2and3SortingTest {
    //----- Maybe you'll find that useful for ex. (3)... -------
    @FunctionalInterface
    interface ISorting {
        void sort(int[] t);
    }

    static final ISorting[] algos = {
        BuggySorting::sort00, BuggySorting::sort01,
        BuggySorting::sort02, BuggySorting::sort03,
        BuggySorting::sort04, BuggySorting::sort05,
        BuggySorting::sort06, BuggySorting::sort07,
        BuggySorting::sort08, BuggySorting::sort09,
        BuggySorting::sort10, BuggySorting::sort11,
        Arrays::sort
    };

    //=============================================================

    public static boolean isSortingResultCorrect(int[] orig, int[] sorted) {
        // if the size is different, the result is automatically false
        if (orig.length != sorted.length) {
            return false;
        }

        for (int i = 0; i < sorted.length; i++) {
            // check if the index is the last
            if (i != sorted.length - 1) {
                // check if the sorted array is sorted
                if (sorted[i] > sorted[i + 1]) {
                    return false;
                }
            }

            // check if occurences in sorted array are equals to occurences in original array
            if (nbOfOccurrences(sorted, sorted[i]) != nbOfOccurrences(sorted, orig[i])) {
                return false;
            }
        }
        return true;
    }

    // Maybe you'll find such a method useful...
    private static int nbOfOccurrences(int[] t, int e) {
        int occurences = 0;
        for (int currentValue : t) {
            if (currentValue == e) {
                occurences++;
            }
        }
        return occurences;
    }

    // ------------------------------------------------------------
    public static void main(String[] args) {
        int[][] arrayTest = { {3, 5, -2, 11, 0, 12, 1, -1, 2, -3, 6, 7, 8, 4, 9, 10}, { 0, 0, 0, 0 }, {5, 4, 3, 2, 1}, {} };
        int[][] sortedArrayTest = { {-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}, { 0, 0, 0, 0 }, {1, 2, 3, 4, 5}, {} };


        int indexAlgorithm = 0;

        for (int i = 0; i < arrayTest.length; ++i) {
            System.out.println("Array number: " + i);
            for (ISorting a : algos) {
                // We copy the original array because else, it's gonna be modified by the sorting algorithm
                int[] arrayToSort = Arrays.copyOf(arrayTest[i], arrayTest[i].length);
                a.sort(arrayToSort);
                // Display the results of the sorting algorithm :
                System.out.println("Algo nbr: " + (++indexAlgorithm) + " has sorted array: "
                    + isSortingResultCorrect(arrayToSort, sortedArrayTest[i]));
            }
            indexAlgorithm = 0;
        }
    }
}
