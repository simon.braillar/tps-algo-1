package s01;

public class CharStack {
    private int topIndex;
    private char[] buffer;
    private static final int DEFAULT_SIZE = 10;

    // Default constructor using the default size for the buffer
    public CharStack() {
        this(DEFAULT_SIZE);
    }

    // Constructor with buffer size in parameter
    public CharStack(int estimatedSize) {
        buffer = new char[estimatedSize];
        topIndex = -1;
    }

    // Check if the stack is empty
    public boolean isEmpty() {
        boolean isEmpty = false;
        if (buffer[0] == 0) {
            isEmpty = true;
        }
        return isEmpty;
    }

    // Get the top char of the stack
    public char top() {
        return buffer[topIndex];

    }

    // Remove the top char of the stack
    public char pop() {
        char top = 0;
        if (!isEmpty()) {
            top = buffer[topIndex];
        }
        buffer[topIndex] = 0;
        topIndex -= 1;
        return top;
    }

    // Add a char at the top of the stack
    public void push(char x) {
        if ((topIndex + 1) >= buffer.length) {
            char[] newBuffer = new char[buffer.length * 2];
            for (int i = 0; i < buffer.length; i++) {
                newBuffer[i] = buffer[i];
            }
            buffer = newBuffer;
        }
        buffer[topIndex + 1] = x;
        topIndex += 1;
    }

    // Getters only used for testing
    public char[] getBuffer() {
        return buffer;
    }

    public int getTopIndex() {
        return topIndex;
    }
}
