package s13;

import java.util.Random;

public class RndLinear {
    public static void main(String[] args) {
        int nbOfExperiments = 100000;
        int n = 10;
        Random r = new Random();
        testLinear(r, n, nbOfExperiments);
    }

    //============================================================
    public static int rndLinear(Random r, int n) {
        int sumElements = (n * (n + 1) / 2); // if n=5, sumElements = 1+2+3+4+5 = 15
        int rnd = r.nextInt(sumElements);
        int j = n;

        for (int i = n; i > 0; i--) {
            if (rnd < j) {
                return i;
            } else {
                j += (i - 1);
            }
        }
        return 0;
    }

    //============================================================
    static void testLinear(Random r, int n, int nbOfExperiments) {
        int[] t = new int[n + 1];
        for (int i = 0; i < nbOfExperiments; i++)
            t[rndLinear(r, n)]++;
        System.out.println(0 + " : " + t[0]);
        for (int i = 1; i < n + 1; i++)
            System.out.println(i + " : " + (double) t[i] / nbOfExperiments);
    }
}
