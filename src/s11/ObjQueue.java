package s11;

import java.util.Random;

public class ObjQueue {
  // ======================================================================
  private static class ObjQueueNode {
    final Object elt;
    ObjQueueNode queueNext = null;

    // ----------
    ObjQueueNode(Object elt) {
      this.elt = elt;
    }

  }

  // ======================================================================
  private ObjQueueNode queueFront;
  private ObjQueueNode queueBack;

  // ------------------------------
  public ObjQueue() {
  }

  // --------------------------
  public void enqueue(Object elt) {
    ObjQueueNode node = new ObjQueueNode(elt);

    if (isEmpty()) {
      queueBack = node;
      queueFront = node;
    } else {
      this.queueBack.queueNext = node;
      queueBack = node;
    }

  }

  // --------------------------
  public boolean isEmpty() {
    return queueBack == null;
  }

  // --------------------------
  // PRE : !isEmpty()
  public Object consult() {
    return queueFront.elt;
  }

  // --------------------------
  // PRE : !isEmpty()
  public Object dequeue() {
    Object e = queueFront.elt;
    if (queueFront == queueBack) {
      queueBack = null;
      queueFront = null;
    } else {
      queueFront = queueFront.queueNext;
    }
    return e;
  }

  // --------------------------
  public String toString() {
    String res = "";
    ObjQueueNode c = queueFront;
    while (c != null) {
      res += c.elt + " ";
      c = c.queueNext;
    }
    return res;
  }

  public static void main(String[] args) {
    int n = 10_000, testRuns = 100;
    if (args.length == 1)
      n = Integer.parseInt(args[0]);
    Random r = new Random();
    long seed = r.nextInt(1000);
    r.setSeed(seed);
    System.out.println("Using seed " + seed);
    while (testRuns-- > 0) {
      ObjQueue q = new ObjQueue();
      int m = 0;
      int k = 0;
      int p = 0;
      for (int i = 0; i < n; i++) {
        boolean doAdd = r.nextBoolean();
        if (doAdd) {
          k++;
          q.enqueue(k);
          ok(!q.isEmpty(), "should be non-empty " + m + " " + k + " " + p + "\n");
          m++;
          //System.out.print("a("+k+")");
        } else {
          if (m == 0) {
            ok(q.isEmpty(), "should be empty " + m + " " + k + " " + p + "\n");
          } else {
            ok(!q.isEmpty(), "should be non-empty " + m + " " + k + " " + p + "\n");
            int e = (int) q.dequeue();
            //System.out.print("r("+e+")");
            m--;
            ok(e == p + 1, "not FIFO " + m + " " + k + " " + p + "\n");
            p++;
          }
        }
      }
    }
    System.out.println("Test passed successfully");
  }

  //------------------------------------------------------------
  static void ok(boolean b, String s) {
    if (b) return;
    throw new RuntimeException("property not verified: " + s);
  }
}

