package s14;

public interface SetOfStringsItr {
  public boolean hasMoreElements();
  // PRE: hasMoreElements()
  public String nextElement();
}
