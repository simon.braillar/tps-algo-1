package s05;

public class RecursivityExos {

    public static int factorial(int x) {
        if (x == 0) {
            return 1;
        }
        return x * factorial(x - 1);
    }

    public static int modulo(int x, int y) {
        if (x < y) return x;
        return modulo(x - y, y);
    }

    public static int square(int n) {
        if (n == 0) return 0;
        return square(n - 1) + 2 * n - 1;
    }

    public static int nbOf1sInBinary(int x) {
        if (x == 0) return 0;
        return x % 2 + nbOf1sInBinary(x / 2);
    }

    public static int fibonacci(int n) {
        if (n < 2) return n;
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    // ----------------------------------------
    // ----------------------------------------
    public static void main(String[] args) {
        int n = 7;
        System.out.println(modulo(n, 6));       // 1
        System.out.println(factorial(n));       // 5040
        System.out.println(nbOf1sInBinary(n));  // 3
        System.out.println(fibonacci(n));       // 13
        System.out.println(square(n));          // 49
    }
}
