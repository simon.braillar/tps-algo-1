package s06;

public class ShortToStringMap {
    private int size;
    private short[] keys;
    private String[] images;
    private static final int DEFAULT_ARRAY_SIZE = 10;

    //------------------------------
    //  Private methods
    //------------------------------

    // Could be useful, for instance :
    // - one method to detect and handle the "array is full" situation
    // - one method to locate a key in the array
    //   (to be called from containsKey(), put(), and remove())

    private void checkArraySize() {
        if (keys.length == size) {
            short[] newKeys = new short[2 * keys.length];
            String[] newImages = new String[2 * keys.length];

            // Put keys and images in the new arrays
            // I could have used Arrays.copyOf but I thought of it too late
            for (int i = 0; i < keys.length; i++) {
                newKeys[i] = keys[i];
                newImages[i] = images[i];
            }

            // Adding the new arrays as attributes
            keys = newKeys;
            images = newImages;
        }
    }

    // Get the key's index if the key already exists
    // Return -1 if the key doesn't exist
    private int getKeyPosition(short key) {
        int position = -1;
        for (int i = 0; i < size; i++) {
            if (key == keys[i]) {
                position = i;
                break;
            }
        }
        return position;
    }

    //------------------------------
    //  Public methods
    //------------------------------
    public ShortToStringMap() {
        size = 0;
        keys = new short[DEFAULT_ARRAY_SIZE];
        images = new String[DEFAULT_ARRAY_SIZE];
    }

    // adds an entry in the map, or updates it
    public void put(short key, String img) {
        checkArraySize();

        // If key already exists
        int position = getKeyPosition(key);
        if (position != -1) {
            images[position] = img;
        } else {
            keys[size] = key;
            images[size] = img;
            size++;
        }
    }

    // returns null if !containsKey(key)
    public String get(short key) {
        String result = null;
        int position = getKeyPosition(key);

        if (position != -1) {
            result = images[position];
        }

        return result;
    }

    public void remove(short e) {
        int position = getKeyPosition(e);

        if (position != -1) {
            keys[position] = keys[size - 1];
            images[position] = images[size - 1];
            size--;
        }
    }

    public boolean containsKey(short k) {
        return getKeyPosition(k) > -1;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    // Inner class for the iterator
    private class ShortToStringMapItr implements s06.ShortToStringMapItr {

        private int index;
        private ShortToStringMap m;

        public ShortToStringMapItr(ShortToStringMap m) {
            this.m = m;
        }

        @Override
        public boolean hasMoreKeys() {
            return index < m.size();
        }

        @Override
        public short nextKey() {
            return m.keys[index++];
        }
    }

    public ShortToStringMapItr iterator() {
        return new ShortToStringMapItr(this);
    }

    // a.union(b) :        a becomes "a union b"
    //  values are those in b whenever possible
    public void union(ShortToStringMap m) {
        for (int i = 0; i < m.size(); i++) {
            this.put(m.keys[i], m.images[i]);
        }
    }

    // a.intersection(b) : "a becomes a intersection b"
    //  values are those in b
    public void intersection(ShortToStringMap s) {
        for (int i = 0; i < this.size; i++) {
            if (!s.containsKey(this.keys[i])) {
                this.remove(this.keys[i]);
                // Returns to the first index of the loop on the next pass
                i = -1;
            } else {
                this.put(this.keys[i], s.get(this.keys[i]));
            }
        }
    }

    // a.toString() returns all elements in
    // a string like: {3:"abc",9:"xy",-5:"jk"}
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("{");

        for (int i = 0; i < size; i++) {
            strBuilder.append(keys[i] + ":" + images[i]);
            if (i != size - 1) {
                strBuilder.append(",");
            }
        }
        strBuilder.append("}");
        return strBuilder.toString();
    }
}
