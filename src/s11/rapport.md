# Algorithmique série 11 
### Braillard, Dupasquier et Grossenbacher
## Ex 1 classe IntQueueArray
### dequeue()
```java
public int dequeue() {
    int frontValue = consult();
    if (front == buffer.length - 1) {
        front = 0;
    } else {
        front++;
    }
    size--;
    return frontValue;
}
```
### checkSize()
```java
private void checkSize() {
    if (size < buffer.length) return;
    int[] newBuffer = new int[buffer.length * 2]; 
	int index = 0;
    for (int i = front; i < buffer.length; i++) {
        newBuffer[index++] = buffer[i];
    }
    for (int i = 0; i < front; i++) {
        newBuffer[index++] = buffer[i];
    }
    buffer = newBuffer;
    front = 0;
    back = size - 1;
}
```
### Output :
```text
Using seed 38
Test passed successfully

Process finished with exit code 0
```
## Ex 2 classe IntQueueChained
### enqueue()
```java
public void enqueue(int elt) {
    QueueNode newQueueNode = new QueueNode(elt);
    if (isEmpty()) {
        back = newQueueNode;
        front = newQueueNode;
    } else {
        back.next = newQueueNode;
        back = newQueueNode;
    }
}
```
### Output :
```text
Using seed 21
Test passed successfully

Process finished with exit code 0
```

## Ex 3

### Classe QueueChained
```java
package s11;

import java.util.Random;

public class QueueChained<E> {
  // ======================================================================
  private static class QueueNode<E> {
    final E elt;
    QueueNode<E> next = null;
    QueueNode(E elt) {
      this.elt = elt;
    }
  }

  // ======================================================================
  private QueueNode<E> queueFront;
  private QueueNode<E> queueBack;

  // ------------------------------
  public QueueChained() {
  }

  // --------------------------
  public void enqueue(E elt) {
    QueueNode<E> queueNode = new QueueNode<E>(elt);

    if (isEmpty()) {
      queueBack = queueNode;
      queueFront = queueNode;
    } else {
      this.queueBack.next = queueNode;
      queueBack = queueNode;
    }

  }

  // --------------------------
  public boolean isEmpty() {
    return queueBack == null;
  }

  // --------------------------
  // PRE : !isEmpty()
  public E consult() {
    return queueFront.elt;
  }

  // --------------------------
  // PRE : !isEmpty()
  public E dequeue() {
    E e = queueFront.elt;
    if (queueFront == queueBack) {
      queueBack = null;
      queueFront = null;
    } else {
      queueFront = queueFront.next;
    }
    return e;
  }

  // --------------------------
  public String toString() {
    String res = "";
    QueueNode<E> c = queueFront;
    while (c != null) {
      res += c.elt + " ";
      c = c.next;
    }
    return res;
  }

  public static void main(String[] args) {
    int n = 10_000, testRuns = 100;
    if (args.length == 1)
      n = Integer.parseInt(args[0]);
    Random r = new Random();
    long seed = r.nextInt(1000);
    r.setSeed(seed);
    System.out.println("Using seed " + seed);
    while (testRuns-- > 0) {
      QueueChained<Integer> q = new QueueChained<>();
      int m = 0;
      int k = 0;
      int p = 0;
      for (int i = 0; i < n; i++) {
        boolean doAdd = r.nextBoolean();
        if (doAdd) {
          k++;
          q.enqueue(k);
          ok(!q.isEmpty(), "should be non-empty " + m + " " + k + " " + p + "\n");
          m++;
          //System.out.print("a("+k+")");
        } else {
          if (m == 0) {
            ok(q.isEmpty(), "should be empty " + m + " " + k + " " + p + "\n");
          } else {
            ok(!q.isEmpty(), "should be non-empty " + m + " " + k + " " + p + "\n");
            int e = q.dequeue();
            //System.out.print("r("+e+")");
            m--;
            ok(e == p + 1, "not FIFO " + m + " " + k + " " + p + "\n");
            p++;
          }
        }
      }
    }
    System.out.println("Test passed successfully");
  }

  //------------------------------------------------------------
  static void ok(boolean b, String s) {
    if (b) return;
    throw new RuntimeException("property not verified: " + s);
  }

}
```

#### Démo pour la classe QueueChained :

```java
public class Demo {
  static void demo(int n) {
    QueueChained<Integer> f;
    int i, sum=0;
    f = new QueueChained<Integer>();
    for (i=0; i<n; i++)
      f.enqueue(i);
    while(! f.isEmpty())
      sum = sum + f.dequeue();
    System.out.println(sum);
  }
}
```

#### Output :
```
Using seed 404
Test passed successfully

Process finished with exit code 0
```

### Classe ObjQueue
```java
public class ObjQueue {
  // ======================================================================
  private static class ObjQueueNode {
    final Object elt;
    ObjQueueNode queueNext = null;

    // ----------
    ObjQueueNode(Object elt) {
      this.elt = elt;
    }

  }

  // ======================================================================
  private ObjQueueNode queueFront;
  private ObjQueueNode queueBack;

  // ------------------------------
  public ObjQueue() {
  }

  // --------------------------
  public void enqueue(Object elt) {
    ObjQueueNode node = new ObjQueueNode(elt);

    if (isEmpty()) {
      queueBack = node;
      queueFront = node;
    } else {
      this.queueBack.queueNext = node;
      queueBack = node;
    }

  }

  // --------------------------
  public boolean isEmpty() {
    return queueBack == null;
  }

  // --------------------------
  // PRE : !isEmpty()
  public Object consult() {
    return queueFront.elt;
  }

  // --------------------------
  // PRE : !isEmpty()
  public Object dequeue() {
    Object e = queueFront.elt;
    if (queueFront == queueBack) {
      queueBack = null;
      queueFront = null;
    } else {
      queueFront = queueFront.queueNext;
    }
    return e;
  }

  // --------------------------
  public String toString() {
    String res = "";
    ObjQueueNode c = queueFront;
    while (c != null) {
      res += c.elt + " ";
      c = c.queueNext;
    }
    return res;
  }

  public static void main(String[] args) {
    int n = 10_000, testRuns = 100;
    if (args.length == 1)
      n = Integer.parseInt(args[0]);
    Random r = new Random();
    long seed = r.nextInt(1000);
    r.setSeed(seed);
    System.out.println("Using seed " + seed);
    while (testRuns-- > 0) {
      ObjQueue q = new ObjQueue();
      int m = 0;
      int k = 0;
      int p = 0;
      for (int i = 0; i < n; i++) {
        boolean doAdd = r.nextBoolean();
        if (doAdd) {
          k++;
          q.enqueue(k);
          ok(!q.isEmpty(), "should be non-empty " + m + " " + k + " " + p + "\n");
          m++;
          //System.out.print("a("+k+")");
        } else {
          if (m == 0) {
            ok(q.isEmpty(), "should be empty " + m + " " + k + " " + p + "\n");
          } else {
            ok(!q.isEmpty(), "should be non-empty " + m + " " + k + " " + p + "\n");
            int e = (int) q.dequeue();
            //System.out.print("r("+e+")");
            m--;
            ok(e == p + 1, "not FIFO " + m + " " + k + " " + p + "\n");
            p++;
          }
        }
      }
    }
    System.out.println("Test passed successfully");
  }

  //------------------------------------------------------------
  static void ok(boolean b, String s) {
    if (b) return;
    throw new RuntimeException("property not verified: " + s);
  }
}
```

#### Classe Demo pour ObjQueue :
```
public class Demo {
  static void demo(int n) {
    ObjQueue f;
    int i, sum=0;
    f = new ObjQueue();
    for (i=0; i<n; i++)
      f.enqueue(i);
    while(! f.isEmpty())
      sum = sum + (int) f.dequeue();
    System.out.println(sum);
  }

}
```

#### Output : 
```
Using seed 617
Test passed successfully

Process finished with exit code 0
```