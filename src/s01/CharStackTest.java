package s01;

public class CharStackTest {
    public static void main(String[] args) {
        // Creating the stack with a size of 3
        CharStack charStack = new CharStack(3);
        showCharStackInfo(charStack);

        // Pushing 3 chars in the buffer
        charStack.push('a');
        charStack.push('b');
        charStack.push('c');
        showCharStackInfo(charStack);

        // Pushing one more char to check if the size of the buffer is 2x bigger
        charStack.push('d');
        showCharStackInfo(charStack);

        // Removing a char
        charStack.pop();
        showCharStackInfo(charStack);

        // Removing all chars
        charStack.pop();
        charStack.pop();
        charStack.pop();
        showCharStackInfo(charStack);
    }

    // Show all the information needed to check that the stack is working properly
    public static void showCharStackInfo(CharStack charStack) {
        System.out.println("--------------------");
        System.out.println("TopIndex: " + charStack.getTopIndex());
        System.out.println("IsEmpty: " + charStack.isEmpty());
        System.out.println("BufferLength: " + charStack.getBuffer().length);
        System.out.print("BufferContent: ");
        for (int i = 0; i <= charStack.getTopIndex(); i++) {
            System.out.print(charStack.getBuffer()[i] + " ");
        }
        System.out.println();
        System.out.println("--------------------");
    }
}
