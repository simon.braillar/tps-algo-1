package s12;
public interface BitSetOfShortsItr {
  public boolean hasMoreElements();
  // PRE: hasMoreElement()
  public short nextElement();
}
