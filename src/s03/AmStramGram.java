package s03;

public class AmStramGram {

    public static void main(String[] args) {
        int n = 420, k = 25;
        if (args.length == 2) {
            n = Integer.parseInt(args[0]);
            k = Integer.parseInt(args[1]);
        }
        System.out.println("Winner is " + winnerAmStramGram(n, k));
    }

    public static int winnerAmStramGram(int n, int k) {
        List l = new List();
        ListItr li = new ListItr(l);
        int i;
        // Add 1 to n into the list
        while (n > 0) {
            li.insertAfter(n--);
        }
        while (l.size > 1) {
            // Iterate to k
            for (i = 1; i < k; ++i) {
                li.goToNext();
                if (li.isLast()) {
                    li.goToFirst();
                }
            }
            li.removeAfter();
            // If remove was the last, go to the first element
            if (li.isLast()) {
                li.goToFirst();
            }
        }
        return li.consultAfter();
    }
}

